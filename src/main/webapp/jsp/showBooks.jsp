<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of books</title>
</head>
<body>
	<h1>List of Books</h1>
	<table border="1">
		<tr>
			<th>Book ID</th>
			<th>Title</th>
			<th>Author</th>
		</tr>
		<tr>
			<c:forEach items="${books}" var="book">
				<tr>
					<td>${book.bid}</td>
					<td>${book.title}</td>
					<td>${book.author}</td>
				</tr>
			</c:forEach>
		</tr>
	</table>

<a href="/">Home</a>
		<a href="/addBook">Add Book</a>
			<a href="/showBooks">List Books</a>
		<a href="/showCustomers">List Customers</a>
		<a href="/addCustomer">Add Customers</a>
		<a href="/newLoan">New Loan</a>
		<a href="/showLoans">List Loans</a>
</body>
</html>