package com.sales.resources;

public final class Messages {
	public static final String ERR_NOT_EMPTY = "May not be empty1";
	public static final String ERR_MUST_BE_GREATER = "Must be greater or equal to 1!";
	public static final String ERR_CREATE_LOAN = "Could not create new Loan!";
	public static final String ERR_DELETE_LOAN = "Could not delete loan!";
	public static final String ERR_NO_LOAN = "No such loan: ";
}
