package com.sales.exceptions;

public class NotAvailableException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title;
	public NotAvailableException(String title, String msg) {
		super(msg);
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
}