package com.sales.exceptions;

public class NotExistException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title;
	
	public NotExistException(String title, String msg) {
		super(msg);
		this.title = title;
	}
	
	public String getTitle() {
		return this.title;
	}
}
