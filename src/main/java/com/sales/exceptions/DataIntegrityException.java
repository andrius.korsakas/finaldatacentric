package com.sales.exceptions;

import java.sql.SQLException;

public class DataIntegrityException extends SQLException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataIntegrityException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

}
 