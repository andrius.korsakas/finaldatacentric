package com.sales.generators;

import java.time.LocalDate;

import org.hibernate.Session;
import org.hibernate.tuple.ValueGenerator;

import com.sales.models.Loan;

public class DueDateGenerator implements ValueGenerator<String> {
	
	@Override
	public String generateValue(Session session, Object owner) {
		Loan loan = (Loan) owner;
		long loanPeriod = loan.getCust().getLoanPeriod();
		return LocalDate.now().plusDays(loanPeriod).toString();
	}

}
