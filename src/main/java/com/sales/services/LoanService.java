package com.sales.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sales.models.Loan;
import com.sales.repositories.BookOnLoan;
import com.sales.repositories.LoanRepository;

@Service
public class LoanService {
	@Autowired
	LoanRepository loanRep;
	
	public ArrayList<Loan> getAllLoans(){
		return (ArrayList<Loan>) loanRep.findAll();
	}
	
	public Loan save(Loan loan){
			return loanRep.save(loan);
	}
	
	public boolean customerExists(Long id) {
		return loanRep.findCustomerById(id).size() > 0;
	}
	
	public boolean bookExists(Long id) {
		return loanRep.findBookById(id).size() > 0;
	}
	
	public List<BookOnLoan> getBook(Long id) {
		return loanRep.bookOnLoan(id);
	}
	
	public void deleteLoan(Long id) {
		loanRep.delete(id);
	}
	
	public long getLoanPeriod(long id) {
		return loanRep.getLoanPeriod(id);
	}
}
