package com.sales.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sales.models.Book;
import com.sales.repositories.BookRepository;

@Service
public class BooksService {
	
	@Autowired
	BookRepository bookRep;
	
	public List<Book> getAllBooks(){
		return (List<Book>) bookRep.findAll();
	}
	
	public Book save(Book book) {
		return bookRep.save(book);
	}
	
	public boolean bookExists(long id) {
		return bookRep.exists(id);
	}
}
