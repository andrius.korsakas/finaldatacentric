package com.sales.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sales.models.Customer;
import com.sales.repositories.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	CustomerRepository customerRep;
	
	public List<Customer> getAllCustomers(){
		return (List<Customer>) customerRep.findAll();
	}
	
	public Customer save(Customer c) {
		return customerRep.save(c);
	}
	
	public boolean customerExists(long id) {
		return customerRep.exists(id);
	}
}
