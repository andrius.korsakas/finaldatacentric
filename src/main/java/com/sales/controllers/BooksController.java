package com.sales.controllers;

import java.util.ArrayList;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.sales.models.Book;
import com.sales.services.BooksService;

@Controller
@EnableWebSecurity
public class BooksController{

  @Autowired
  BooksService booksService;
  
  @RequestMapping(value = "/showBooks", method=RequestMethod.GET)
  public String showBooks(Model model) {
    ArrayList<Book> books = (ArrayList<Book>) booksService.getAllBooks();
    model.addAttribute("books", books);
    return "showBooks";
  }

  @RequestMapping(value = "/addBook", method=RequestMethod.GET)
  public String addBookGET(Model model) {
    Book book = new Book();
    model.addAttribute("book", book);
    return "addBook";
  }

  @RequestMapping(value = "/addBook", method=RequestMethod.POST)
  public String addBookPOST(@Valid @ModelAttribute("book") Book book, BindingResult bindingResult, Model model) {
    if (bindingResult.hasErrors()) {
      return "addBook";
    }
    
    booksService.save(book);
    return "redirect:showBooks";
  }
}
