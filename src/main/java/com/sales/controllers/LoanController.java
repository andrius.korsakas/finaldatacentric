package com.sales.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import com.sales.exceptions.NotAvailableException;
import com.sales.exceptions.NotExistException;
import com.sales.models.Loan;
import com.sales.repositories.BookOnLoan;
import com.sales.resources.Messages;
import com.sales.services.LoanService;

@Controller
public class LoanController {
	@Autowired
	LoanService loanService;

	@GetMapping("/showLoans")
	public String showLoans(Model model) {
		ArrayList<Loan> loans = (ArrayList<Loan>) loanService.getAllLoans();
		model.addAttribute("loans", loans);

		return "showLoans";
	}

	@GetMapping("/newLoan")
	public String addLoan(Model model) {
		Loan loan = new Loan();
		model.addAttribute("loan", loan);

		return "newLoan";
	}

	@PostMapping("/newLoan")
	public String newLoan(@ModelAttribute("loan") Loan loan, Model model) {
		loan.getCust().setLoanPeriod((int) loanService.getLoanPeriod(loan.getCust().getcId()));
		
		if (!loanService.customerExists(loan.getCust().getcId())) {
			throw new NotExistException(Messages.ERR_CREATE_LOAN, "Customer with id " + loan.getCust().getcId() +" doesnt exist.");
		}else if(!loanService.bookExists(loan.getBook().getBid())) {
			throw new NotExistException(Messages.ERR_CREATE_LOAN, "Book with id " + loan.getBook().getBid() + " doesnt exist.");
		}else {
			List<BookOnLoan> bookLoan = loanService.getBook(loan.getBook().getBid());
			if(!bookLoan.isEmpty()) { 
				throw new NotAvailableException(Messages.ERR_CREATE_LOAN, "Book: "+loan.getBook().getBid()+ "(" + bookLoan.get(0).getName() +
						") already on loan to Customer " +bookLoan.get(0).getCID()+ "("+ bookLoan.get(0).getTitle()+")"); 
			}
		}
		
		loanService.save(loan);
		return "redirect:showLoans";
	}
	
	@GetMapping("/deleteLoan")
	public String deleteLoan(Model model) {
		Loan loan = new Loan();
		model.addAttribute("loan", loan);
		
		return "deleteLoan";
	}
	
	@PostMapping("/deleteLoan")
	public String deleteLoan(@ModelAttribute("loan") Loan loan, Model model) {
		try {
			loanService.deleteLoan(loan.getLid());
		}catch (Exception e) {
			throw new NotExistException(Messages.ERR_DELETE_LOAN, Messages.ERR_NO_LOAN + loan.getLid());
		}
		return "redirect:showLoans";
	}
}
