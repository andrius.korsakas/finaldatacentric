package com.sales.controllers;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sales.models.Customer;
import com.sales.services.CustomerService;
@Controller
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@RequestMapping(value = "/showCustomers", method=RequestMethod.GET)
	public String showCustomers(Model model) {
		ArrayList<Customer> customers = (ArrayList<Customer>) customerService.getAllCustomers();
		model.addAttribute("customers", customers);
		return "showCustomers";
	}

	@GetMapping("/addCustomer")
	public String addCustomer(Model model) {
		Customer customer = new Customer();
		model.addAttribute("customer", customer);

		return "addCustomer";
	}

	@PostMapping("/addCustomer")
	public String addCustomer(@Valid @ModelAttribute("customer") Customer customer, BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			return "addCustomer";
		}

		customerService.save(customer);
		return "redirect:showCustomers";
	}
	
	@PostMapping("/logout")
	public String logout() {
		return "/";
	}
}
