package com.sales.controllers;

import javax.servlet.http.HttpServletRequest;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.sales.exceptions.NotAvailableException;
import com.sales.exceptions.NotExistException;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest req, Exception exception)
			throws Exception {

		if (AnnotationUtils.findAnnotation(exception.getClass(),
				ResponseStatus.class) != null)
			throw exception;

		ModelAndView mav = new ModelAndView();
		mav.addObject("message", exception.getMessage());
		mav.addObject("title", "Something went wrong!");
		if(exception instanceof NotExistException) {
			mav.addObject("title", ((NotExistException) exception).getTitle());
		}
		if(exception instanceof NotAvailableException) {
			mav.addObject("title", ((NotAvailableException) exception).getTitle());
		}
		mav.setStatus(HttpStatus.BAD_REQUEST);
		mav.setViewName("/error");
		return mav;
	}
}
