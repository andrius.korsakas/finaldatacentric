package com.sales.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sales.models.Loan;

@Repository
public interface LoanRepository extends CrudRepository<Loan, Long>{
	
	@Query(value = "SELECT CID FROM customers where cid = :id", nativeQuery = true)
	public List<Object> findCustomerById(@Param("id") Long id);
	
	@Query(value = "SELECT BID FROM books where bid = :id", nativeQuery = true)
	public List<Object> findBookById(@Param("id") Long id);
	
	@Query(value = "SELECT customers.cid as CID, books.title as Title, customers.cname as Name from "
			+ "loans inner join books on books.bid = loans.bid inner join customers "
			+ "on customers.cid = loans.cid where loans.bid = :id", nativeQuery = true)
	public ArrayList<BookOnLoan> bookOnLoan(@Param("id") Long id);

	@Query(value = "Select loanperiod from customers where cid = :id", nativeQuery = true) 
	public long getLoanPeriod(@Param("id")Long id);
}
