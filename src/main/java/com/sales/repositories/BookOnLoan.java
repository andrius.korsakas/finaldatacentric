package com.sales.repositories;

import javax.persistence.Column;

public interface BookOnLoan {
	@Column(name="CID")
	Integer getCID();
	@Column(name="Title")
	String getTitle();
	@Column(name="Name")
	String getName();
}
