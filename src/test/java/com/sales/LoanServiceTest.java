package com.sales;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import java.time.LocalDate;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import com.sales.controllers.LoanController;
import com.sales.models.Book;
import com.sales.models.Customer;
import com.sales.models.Loan;
import com.sales.repositories.BookOnLoan;
import com.sales.services.LoanService;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
@ContextConfiguration(classes = {LoanController.class, LoanService.class} ) 
public class LoanServiceTest {

	@Autowired
	private LoanService repository;

	@Test public void testGetAllLoans() {
		assertThat(this.repository.getAllLoans()).hasAtLeastOneElementOfType(Loan.class);
	}

	@Test public void testSave() { 
		Loan loan = new Loan(); 
		Customer c = new Customer(); 
		c.setcName("BOB BOV"); 
		c.setLoanPeriod(1);
		Book b = new Book();
		b.setAuthor("a");
		b.setTitle("a");
		loan.setCust(c); 
		loan.setBook(b);
		assertThat(this.repository.save(loan)).isInstanceOf(Loan.class).hasFieldOrPropertyWithValue("dueDate",
				LocalDate.now().plusDays(1).toString()); }

	@Test public void testCustomerExists() {
		assertTrue(this.repository.customerExists(1L)); 
	}

	@Test public void testBookExists() {
		assertTrue(this.repository.bookExists(104l)); }

	@Test public void testGetBook() { List<BookOnLoan> b =
			this.repository.getBook(104L); assertThat(b).hasSize(1); }

	@Test(expected = Exception.class) 
	public void testDeleteLoan() throws Exception {
		this.repository.deleteLoan(300l);
	}

	@Test
	public void testGetLaonPeriod() {
		assertThat(this.repository.getLoanPeriod(1l)).isEqualTo(10);
	}

}
