package com.sales;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.sales.models.Loan;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LoanControllerTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@LocalServerPort
    private int port;
	
//	@Test
	public void testShowLoans() throws Exception {
		  assertThat(this.restTemplate.getForObject("http://localhost:" + port +
		  "/showLoans", String.class)).contains("Loan ID");
	}

	//@Test
	public void testAddLoan() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port +
				  "/newLoan", String.class)).contains("New Loan");
	}

//	@Test
	public void testNewLoan() {
		Loan loan = new Loan();
		assertThat(this.restTemplate.postForEntity("http://localhost:" + port +
				  "/newLoan", null, String.class)).asString().contains("Loan");
	}
	
//	@Test
	public void testDeleteLoan() {
		Loan l = new Loan();
		l.setLid(3000l);
		assertThat(this.restTemplate.postForEntity("http://localhost:" + port +
				  "/newLoan", l, String.class)).asString().contains("No such loan");
	}

}
